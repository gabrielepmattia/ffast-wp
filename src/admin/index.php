<?php
// =============================================
//  index.php
// =============================================

/**
 * @Project:      ffast
 * @Autor:        Gabriele Proietti Mattia <gabry3795>
 * @Email:        gabry.gabry@hotmail.it
 * @Created on:   Feb 04, 2017
 * @Modified by:  gabry3795
 * @Modified on:  Feb 18, 2017
 * @License:      GNU Public License 3.0
 * @Copyright:    See /LICENSE for full license text
 */

add_action('admin_menu', 'my_plugin_menu');

function my_plugin_menu() {
	// register page
	add_menu_page('Dashboard', 'FFast', 'manage_options', __FILE__, 'my_setting_page_dashboard');
	// register settings
	add_action( 'admin_init', 'register_ffast_main_settings' );
}

/** Step 3. */
function my_setting_page_dashboard() {
	if ( !current_user_can( 'manage_options' ) )  {
		wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
	}

	$structure = ABSPATH . "wp-content/ffast";
  if(!file_exists($structure)) mkdir($structure, 0644, true);
?>

  <div class="wrap">

	  <h1>FFast</h1>
	  <p>This is the plugin dashboard.</p>

	  <h2>Requirements</h2>
	  <p>Directory <pre><?php echo $structure ?> </pre> is <?php echo is_writable($structure) === true ? "" : "<span style=\"color:red\"><b>not</b></span>" ?> writeable (if not can be also absent).</p>
		<h2>Settings</h2>
		<form method="post" action="options.php">
		    <?php settings_fields( 'ffast-main-settings' ); ?>
		    <?php do_settings_sections( 'ffast-main-settings' ); ?>
				<table class="form-table">
		        <tr valign="top">
		        <th scope="row">Content start div string</th>
		        <td><input type="text" name="content-div-id" value="<?php echo esc_attr( get_option('content-div-id') ); ?>" /></td>
		        </tr>

		        <tr valign="top">
		        <th scope="row">Content start div tag name</th>
		        <td><input type="text" name="content-div-tagname" value="<?php echo esc_attr( get_option('content-div-tagname') ); ?>" /></td>
		        </tr>
		    </table>
				<?php	submit_button(); ?>
		</form>

	</div>

<?php
}


function register_ffast_main_settings() {
	//register our settings
	register_setting( 'ffast-main-settings', 'content-div-id' );
	register_setting( 'ffast-main-settings', 'content-div-tagname' );
}


// create custom plugin settings menu
//add_action('admin_menu', 'my_cool_plugin_create_menu');

function my_cool_plugin_create_menu() {

	//create new top-level menu
	add_menu_page('My Cool Plugin Settings', 'Cool Settings', 'administrator', __FILE__, 'my_cool_plugin_settings_page' , plugins_url('/images/browser.png', __FILE__) );

	//call register settings function
	add_action( 'admin_init', 'register_my_cool_plugin_settings' );
}


function register_my_cool_plugin_settings() {
	//register our settings
	register_setting( 'my-cool-plugin-settings-group', 'content-start-div-string' );
}

function my_cool_plugin_settings_page() {
?>

<div class="wrap">
<h1>Your Plugin Name</h1>

<form method="post" action="options.php">
    <?php settings_fields( 'my-cool-plugin-settings-group' ); ?>
    <?php do_settings_sections( 'my-cool-plugin-settings-group' ); ?>
    <table class="form-table">
        <tr valign="top">
        <th scope="row">New Option Name</th>
        <td><input type="text" name="new_option_name" value="<?php echo esc_attr( get_option('new_option_name') ); ?>" /></td>
        </tr>

        <tr valign="top">
        <th scope="row">Some Other Option</th>
        <td><input type="text" name="some_other_option" value="<?php echo esc_attr( get_option('some_other_option') ); ?>" /></td>
        </tr>

        <tr valign="top">
        <th scope="row">Options, Etc.</th>
        <td><input type="text" name="option_etc" value="<?php echo esc_attr( get_option('option_etc') ); ?>" /></td>
        </tr>
    </table>
    <!-- http://www.flaticon.com/free-icon/browser_297581#term=browser&page=1&position=62 -->
    <?php submit_button(); ?>

</form>
</div>
<?php } ?>
