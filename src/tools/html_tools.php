<?php
// =============================================
//  html_tools.php
// =============================================

/**
 * @Project:      ffast
 * @Autor:        Gabriele Proietti Mattia <gabry3795>
 * @Email:        gabry.gabry@hotmail.it
 * @Created on:   Feb 18, 2017
 * @Modified by:  gabry3795
 * @Modified on:  Feb 18, 2017
 * @License:      GNU Public License 3.0
 * @Copyright:    See /LICENSE for full license text
 */

 function filter_html($html, $divid, $tagname){
   $dom = new DOMDocument();
   @$dom->loadHTML($html);
   @$div = $dom->getElementById($divid);
   @$content = $dom->saveXML($div, LIBXML_NOEMPTYTAG);
   //$content = substr($content, strpos($content, "\n")); // Remove first line
   $content = preg_replace('/<'. $tagname .' (?=[^>]*id="main")[^>]*>/i', "", $content); // Remove div tag
   $content = substr($content, 0, strrpos($content, "</div>")); // remove div closing

   // Get the title
   $titlelist = $dom->getElementsByTagName("title");
   if($titlelist->length > 0) $title = $titlelist->item(0)->nodeValue;
   else $title = "";

   return  "<html><head><title>". $title ."</title></head><body>" . $content . "</body></html>"; // TODO Add page title
 }
