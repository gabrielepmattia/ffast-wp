<?php
// =============================================
//  ffast.php
// =============================================

/**
 * @Project:      ffast
 * @Autor:        Gabriele Proietti Mattia <gabry3795>
 * @Email:        gabry.gabry@hotmail.it
 * @Created on:   Feb 18, 2017
 * @Modified by:  gabry3795
 * @Modified on:  Feb 18, 2017
 * @License:      GNU Public License 3.0
 * @Copyright:    See /LICENSE for full license text
 */

/**
 * @package FFast
 * @version 0.1
 */

/*
Plugin Name: FFast
Plugin URI: http://wordpress.org/plugins/hello-dolly/
Description: This is a test plugin.
Author: Gabriele Proietti Mattia <gabry.gabry@hotmail.it>
Version: 0.0.1
Author URI: http://gabry3795.gitlab.com/
*/

// deps
require(plugin_dir_path(__FILE__) . "tools/index.php");
require(plugin_dir_path(__FILE__) . "core/index.php");

// Cacher hook
add_action('template_redirect', 'buffer_start', 0);

// Scripts hook
function enqueue_plugin_files() {
  wp_enqueue_script( 'script', plugin_dir_url(__FILE__) . 'js/ffast.js');
}
add_action( 'wp_enqueue_scripts', 'enqueue_plugin_files' );

// Admin section
require(plugin_dir_path(__FILE__) . "admin/index.php");

?>
