// =============================================
//  preloader.js
// =============================================

/**
 * @Project:      ffast
 * @Autor:        Gabriele Proietti Mattia <gabry3795>
 * @Email:        gabry.gabry@hotmail.it
 * @Created on:   Feb 18, 2017
 * @Modified by:  gabry3795
 * @Modified on:  Feb 18, 2017
 * @License:      GNU Public License 3.0
 * @Copyright:    See /LICENSE for full license text
 */

var divid = "main";

function preloadDir(req, complete_href) {
  //var xhttp = window.XMLHttpRequest ? new XMLHttpRequest : new ActiveXObject("Microsoft.XMLHTTP");
  // Save state on leaving if null
  window.history.replaceState({
    "html": document.getElementById(divid).innerHTML,
    "pageTitle": document.title
  }, document.title, window.location);
  // Start ajax request
  var xhttp = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject("Microsoft.XMLHTTP");
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      document.getElementById(divid).innerHTML = this.responseText; // TODO Parametrize! main
      // Change dir and Update link in history
      //console.log("PageTitle is :: " + this.pageTitle);
      console.log("responseText is " + this.responseText);
      window.pageTitle = this.pageTitle;
      window.history.pushState({
        "html": this.responseText,
        "pageTitle": this.pageTitle
      }, this.pageTitle, req);
    } else if (this.readyState == 4 && this.status == 404) {
      window.location = complete_href;
    }
  };
  //console.log("Starting web request for :: " + req + " == " + req.replace(/\?|\//gi, "_"));
  xhttp.open("GET", "./wp-content/ffast/" + req.replace(/\?|\//gi, "_") + ".html", true); // TODO Parametrize?
  xhttp.send();
}

window.onpopstate = function(e) {
  // Load state from history
  if (e.state) {
    document.getElementById(divid).innerHTML = e.state.html;
    document.title = e.state.pageTitle;
  }
};

// Hook here every link
document.addEventListener('click', function(e) {
  // Catch the path
  // Check if it's a link
  if (e.target.tagName !== 'A' || e.metaKey || e.ctrlKey) return;
  // Block its action and call mine
  e.preventDefault();
  var path = e.target.pathname + "" + e.target.search;
  preloadDir(path, e.target.href);
  return false;
}, true);
