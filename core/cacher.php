<?php
// =============================================
//  cacher.php
// =============================================

/**
 * @Project:      ffast
 * @Autor:        Gabriele Proietti Mattia <gabry3795>
 * @Email:        gabry.gabry@hotmail.it
 * @Created on:   Feb 18, 2017
 * @Modified by:  gabry3795
 * @Modified on:  Feb 18, 2017
 * @License:      GNU Public License 3.0
 * @Copyright:    See /LICENSE for full license text
 */

 function callback($buffer) {
     // Resolve resources and beautify here
     return $buffer;
 }

 function buffer_start() {
   // Cache only if user is not logged and we didn't cached the page yet
   if(!is_user_logged_in()){
     add_action('wp_footer', 'buffer_end', PHP_INT_MAX);
     @ob_start('callback');
   }
 }

 function buffer_end() {
   // We have the entire buffer, we can save it
   $buffer = ob_get_contents();
   @ob_end_flush();

   // Filter the inner main div content
   $buffer = filter_html($buffer, get_option('content-div-id'), get_option('content-div-tagname'));

   // Create the structure
   $structure = plugin_dir_path(__FILE__) . "../../../../wp-content/ffast/";
   $dest_file = $structure . str_replace(array("/", "?"), "_", $_SERVER['REQUEST_URI']) . ".html";

   if(!file_exists($structure)) mkdir($structure, 0644, true);
   if(is_writable($structure)) if(!file_exists($dest_file)) file_put_contents($dest_file, $buffer);

 }
