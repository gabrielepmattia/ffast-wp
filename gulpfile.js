// =============================================
//  gulpfile.js
// =============================================

/**
 * @Project:      wpnewspaper
 * @Autor:        Gabriele Proietti Mattia <gabry3795>
 * @Email:        gabry.gabry@hotmail.it
 * @Created on:   Feb 16, 2017
 * @Modified by:  gabry3795
 * @Modified on:  Feb 16, 2017
 * @License:      GNU Public License 3.0
 * @Copyright:    See /LICENSE for full license text
 */

var gulp = require('gulp');
var concat = require('gulp-concat');
var del = require('del');
var replace = require('gulp-replace');
var uglify = require('gulp-uglify');
var run_sequence = require('run-sequence');
var browser_sync = require('browser-sync').create();
// Node
var fs = require('fs');

// ================================ Server side ================================
gulp.task('browser-sync', ['watch'], function() {
  return browser_sync.init(null, {
    proxy: "http://localhost/wp2",
    port: 7000,
    files: ["./*.php", "./js/*.js"]
  });
});

gulp.task('watch', function() {
  gulp.watch('src/js/*.js', ['compile']);
  gulp.watch('src/**/*.php', ['compile-tasks']);
});

gulp.task('serve', ['browser-sync']);
// ================================ Server side ================================

// ================================ Compile side ================================

// ============= Js tasks
gulp.task('js', function() {
  return gulp.src('src/js/*.js')
    .pipe(concat('ffast.js'))
    .pipe(uglify({
      mangle: true,
      compress: {
        sequences: true, // join consecutive statemets with the “comma operator”
        properties: true, // optimize property access: a["foo"] → a.foo
        dead_code: true, // discard unreachable code
        drop_debugger: true, // discard “debugger” statements
        unsafe: false, // some unsafe optimizations (see below)
        conditionals: true, // optimize if-s and conditional expressions
        comparisons: true, // optimize comparisons
        evaluate: true, // evaluate constant expressions
        booleans: true, // optimize boolean expressions
        loops: true, // optimize loops
        unused: true, // drop unused variables/functions
        hoist_funs: true, // hoist function declarations
        hoist_vars: false, // hoist variable declarations
        if_return: true, // optimize if-s followed by return/continue
        join_vars: true, // join var declarations
        cascade: true, // try to cascade `right` into `left` in sequences
        side_effects: true, // drop side-effect-free statements
        warnings: true, // warn about potentially dangerous optimizations/code
        global_defs: {}
      }
    }))
    .pipe(gulp.dest('js/'));
});
gulp.task('js-tasks', ['js']);

// ============= Compile tasks
gulp.task('compile-php', function buildHTML() {
  return gulp.src('src/**/*.php')
    .pipe(gulp.dest('./'));
});
gulp.task('compile-tasks', ['compile-php']);

// ============ Cleaning
var compiled_folders = ['admin', 'js'];
var compiled_filetypes = ['*.php'];
gulp.task('clean', function() {
  del.sync(compiled_folders);
  del.sync(compiled_filetypes);
});

// Tasks for watching
gulp.task('compile', function(callback) {
  run_sequence(
    ['js-tasks'], // Async
    ['compile-tasks'], // Async
    callback);
});

// Running sync
gulp.task('default', function(callback) {
  run_sequence(
    'compile',
    'serve',
    callback);
});
